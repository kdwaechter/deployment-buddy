package com.kodz.buddy.workflows;

import com.kodz.buddy.config.Helpers;
import org.apache.maven.shared.invoker.*;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.kodz.buddy.config.Helpers.getJarName;
import static com.kodz.buddy.config.Helpers.startProcess;

/**
 * Created by Kurt on 11/4/15.
 */
public class Deploy {
    public static List<String> buildHistory = new ArrayList<>();

    public static void buildRepo(String name) throws IOException, InterruptedException, MavenInvocationException {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        String currentDateTimeStamp = dateFormat.format(cal.getTime());
        System.out.println(currentDateTimeStamp + ": repoPushed called for repo " + name);
        if (buildHistory.size() > 5){
            buildHistory.clear();
            System.out.println(currentDateTimeStamp + ": build history purged");
        }
        String jarFilepath = "/repos/" + name + "/target/";
        String pomPath = "/repos/" + name + "/pom.xml";



        System.out.println(" Stopping server and rebuilding...");
        startProcess(Helpers.getStopServerArr());
        startProcess(Helpers.getGitFetchArr(name));
        startProcess(Helpers.getGitPullArr(name));

        InvocationRequest request = new DefaultInvocationRequest();

        request.setPomFile(new File("/repos/" + name + "/pom.xml"));
        request.setGoals(Arrays.asList("clean", "install"));
        Invoker invoker = new DefaultInvoker();
        //invoker.setMavenHome(new File(System.getenv("M2_HOME")));
        invoker.execute(request);


        //start server

        startProcess(Helpers.getStartServerArr(jarFilepath,getJarName(pomPath)));

        System.out.println("Server started again... ");

        buildHistory.add(name + " " + currentDateTimeStamp);
    }

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
