package com.kodz.buddy.workflows;

import com.kodz.buddy.config.HeartBeat;
import com.kodz.buddy.config.Helpers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.Socket;
import java.util.concurrent.TimeUnit;

/**
 * Created by Kurt on 1/12/16.
 */
public class Connect {
    public static Logger logger = LoggerFactory.getLogger(Connect.class);

    public static void reConnect(int portNumber) throws IOException, InterruptedException {
        if (HeartBeat.getIsReconnecting() == false & HeartBeat.getIsReconnectQueued() == true) {
            HeartBeat.setIsReconnecting(true);
            HeartBeat.setIsReconnectQueued(false);
            //attempt to send signal sigterm to currentPortNumber
            disConnect(portNumber);
            //set currentPortNumber = portNumber

            //kick off new process using currentPortNumber
            if (Helpers.getIsWindows()){
                logger.info("Starting OpenVPN...");
                String[] cmd = Helpers.getStartVpnArr(" --config \"C:\\Program Files\\OpenVPN\\config\\client.ovpn\" --auth-user-pass \"C:\\Program Files\\OpenVPN\\config\\credsdca.txt\" --management localhost " + portNumber);
                Helpers.startProcess(cmd);
            }
            else {
                logger.info("Executing external OS request to start for POSIX");
                String[] cmd = Helpers.getStartVpnArr(" --config ~/.ssh/dca-client.ovpn --auth-user-pass ~/.ssh/credsdca --management localhost " + portNumber);
                Helpers.startProcess(cmd);
            }
        }
    }

    public static void disConnect(int portNumber) throws IOException, InterruptedException {

        Socket pingSocket = null;
        PrintWriter out = null;
        BufferedReader in = null;

        try {
            logger.info("Sending shutdown request to local OpenVPN socket interface on port " + portNumber);
            pingSocket = new Socket("localhost", portNumber);
            out = new PrintWriter(pingSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

            out.println("signal SIGTERM");
            TimeUnit.SECONDS.sleep(3);
            System.out.println(in.readLine());
            out.close();
            in.close();
            pingSocket.close();
            logger.info("VPN shutdown sent, sleeping for 10 seconds...");
        } catch (IOException e) {
            logger.info("Could not connect, OpenVPN not running on port.");
            return;
        }


    }

}
