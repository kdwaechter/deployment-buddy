package com.kodz.buddy.config;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.spi.AppenderAttachable;
import com.kodz.buddy.logging.SocketAppender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.TimerTask;

/**
 * Created by master on 1/18/2016.
 */
public class ConnectionTest extends TimerTask {
    public static Logger logger = LoggerFactory.getLogger(ConnectionTest.class);
    public static int failedSubsequentConnectionAttempts = 0;


    public void run(){
        logger.debug("Attempting TCP connection to determine network connectivity...");
        

        try {
            tryConnect();
            HeartBeat.setIsConnected(true);
            HeartBeat.setIsReconnecting(false);
            if (failedSubsequentConnectionAttempts > 0) {
                logger.debug("Successfully reconnected to the network!");
                logger.debug("Setting failed attempt counter from {} back to 0.", failedSubsequentConnectionAttempts);
                failedSubsequentConnectionAttempts = 0;
            }

        } catch (ConnectException e){
            HeartBeat.setIsConnected(false);
            logger.debug("ConnectException caught, unable to attempt socket connection. Subsequent failed attempts: {}", failedSubsequentConnectionAttempts);
            failedSubsequentConnectionAttempts++;
        }
          catch (IOException e){
              HeartBeat.setIsConnected(false);
              logger.debug("Unable to connect within timeout, network connection offline. Subsequent failed attempts: {}", failedSubsequentConnectionAttempts);
              failedSubsequentConnectionAttempts++;
          }
    }

    public static void tryConnect() throws IOException {

        if (failedSubsequentConnectionAttempts >= 10){
            logger.debug("Too many failed attempts in a row, setting reconnecting flag to false.");
            HeartBeat.setIsReconnecting(false);
        }
        Socket socket = new Socket();
        socket.connect(new InetSocketAddress(Configuration.Setting.getRemoteHostDomainForTcpSocket(),
                Configuration.Setting.getRemoteHostPortForTcpSocket()), 1000);
        logger.debug("...connected!");
        socket.close();
    }

}