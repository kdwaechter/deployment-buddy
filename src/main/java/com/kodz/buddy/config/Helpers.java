package com.kodz.buddy.config;


import com.kodz.buddy.Main;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.IOException;

/**
 * Created by Kurt on 11/23/15.
 */
public class Helpers {

    private static boolean isWindows = false;
    static String[] stopServerArr = new String[]{"bash", "-c", "curl -X POST \"http://localhost:" + Main.serverPort + "/utilities/stop\" "};
    static String[] stopServerArrWin = new String[]{"cmd", "/C start /B /normal curl -X POST \"http://localhost:" + Main.serverPort + "/utilities/stop\" "};

    static String[] gitFetchArr = new String[]{"bash", "-c", "cd /repos/%s/ && git fetch "};
    static String[] gitFetchArrWin = new String[]{"cmd", "/C start /B /normal cd /repos/%s/ && git fetch "};

    static String[] gitPullArr = new String[]{"bash", "-c", "cd /repos/%s/ && git pull"};
    static String[] gitPullArrWin = new String[]{"cmd", "/C start /B /normal cd /repos/%s/ && git pull"};

    static String [] startServerArr = new String[]{"bash", "-c", "java -jar %s%s"};
    static String [] startServerArrWin = new String[]{"cmd", "/C start /B /normal java -jar %s%s"};

    static String [] startVpnArr = new String[]{"bash", "-c", "sudo openvpn %s"};
    static String [] startVpnArrWin = new String[]{"cmd", "/C start /B /normal openvpn %s"};

    public static String getResourceFilePath(String relativeFilePath) {

        //Get file from resources folder
        ClassLoader classLoader = Helpers.class.getClassLoader();
        String file = classLoader.getResource(relativeFilePath).getPath();
        return file;

    }

    public static String[] getStopServerArr(){
        return isWindows ? stopServerArrWin : stopServerArr;
    }

    public static String[] getGitFetchArr(String buildName) {
        String[] returnString = isWindows ? gitFetchArrWin : gitFetchArr;
        for(int i = 0; i < returnString.length; i++){
            returnString[i] = String.format(returnString[i], buildName);
        }
        return returnString;
    }

    public static String[] getGitPullArr(String buildName) {
        String[] returnString = isWindows ? gitPullArrWin : gitPullArr;
        for(int i = 0; i < returnString.length; i++){
            returnString[i] = String.format(returnString[i], buildName);
        }
        return returnString;
    }

    public static String[] getStartServerArr(String jarFilePath, String pomPath) {
        String[] returnString = isWindows ? startServerArrWin : startServerArr;
        for(int i = 0; i < returnString.length; i++){
            returnString[i] = String.format(returnString[i], jarFilePath, pomPath);
        }
        return returnString;
    }

    public static String[] getStartVpnArr(String vpnConfigName) {
        String[] returnString = isWindows ? startVpnArrWin : startVpnArr;
        for(int i = 0; i < returnString.length; i++){
            returnString[i] = String.format(returnString[i], vpnConfigName);
        }
        return returnString;
    }

    public static void startProcess(String[] command) throws IOException, InterruptedException {
        ProcessBuilder pb = new ProcessBuilder(command);
        pb.redirectOutput(ProcessBuilder.Redirect.INHERIT);
        pb.redirectError(ProcessBuilder.Redirect.INHERIT);
        pb.start();
    }

    public static String getJarName(String pomFilePath){
        try {
            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(new File(pomFilePath));
            doc.getDocumentElement().normalize();


            String version = doc.getDocumentElement().getElementsByTagName("version").item(0).getFirstChild().getNodeValue();
            String artifactId = doc.getDocumentElement().getElementsByTagName("artifactId").item(0).getFirstChild().getNodeValue();

            return artifactId + "-" + version + ".jar";
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
        return "";
    }

    public static void printConfiguration(){


    }

    public static synchronized void setIsWindows(boolean isWindows) {
        Helpers.isWindows = isWindows;
    }

    public static synchronized Boolean getIsWindows() {
         return Helpers.isWindows;
    }
}
