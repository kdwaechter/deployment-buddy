package com.kodz.buddy.config;

import com.kodz.buddy.dto.Command;
import com.kodz.buddy.dto.ServerSetting;
import org.eclipse.jetty.server.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.TypeDescription;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import java.io.IOException;
import java.net.URL;

/**
 * Created by Kurt on 1/22/16.
 */
public class Configuration {
    public static Logger logger = LoggerFactory.getLogger(Configuration.class);
    static {

        URL resource = Configuration.class.getClassLoader().getResource("settings.yaml");
        Constructor configurationConstructor = new Constructor(Configuration.class);
        TypeDescription serverSettingDescription = new TypeDescription(ServerSetting.class);
        serverSettingDescription.putMapPropertyType("serverSettings", ServerSetting.class, Object.class);
        configurationConstructor.addTypeDescription(serverSettingDescription);

        TypeDescription commandDescription = new TypeDescription(Command.class);
        commandDescription.putMapPropertyType("command", Command.class, Object.class);
        configurationConstructor.addTypeDescription(commandDescription);

            Configuration config = new Configuration();
            Configuration.Setting = new ServerSetting();
            ServerSetting serverSetting = new ServerSetting();
            config.setServerSettings(serverSetting);

            final Configuration configuration = config;
            Configuration.Setting = configuration.getServerSettings();
            Configuration.Command = configuration.getCommands();

    }

    public static ServerSetting Setting;
    public static Command Command;

    private Command commands;
    private ServerSetting serverSettings;

    public  ServerSetting getServerSettings() {
        return serverSettings;
    }

    public void setServerSettings(ServerSetting serverSettings) {
        this.serverSettings = serverSettings;
    }

    public com.kodz.buddy.dto.Command getCommands() {
        return commands;
    }

    public void setCommands(com.kodz.buddy.dto.Command commands) {
        this.commands = commands;
    }

    public static void logSettings(){
        logger.info("Heartbeat Interval: " + Configuration.Setting.getHeartbeatInterval());
        logger.info("Server Port: " + Configuration.Setting.getServicePort());
        logger.info("Remote Tcp Socket Domain: " + Configuration.Setting.getRemoteHostDomainForTcpSocket());
        logger.info("Remote Tcp Socket Port: " + Configuration.Setting.getRemoteHostPortForTcpSocket());
        logger.info("Is Windows: " + Configuration.Setting.isWindows());
    }

}
