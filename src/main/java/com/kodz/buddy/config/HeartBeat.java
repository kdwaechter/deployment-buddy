package com.kodz.buddy.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by master on 1/18/2016.
 */
public class HeartBeat {
    public static Logger logger = LoggerFactory.getLogger(HeartBeat.class);
    private static Timer hearbeatTimer;
    private static Boolean isConnected = false;
    private static Boolean isHeartbeatRunning = false;
    private static Boolean isReconnectQueued = false;
    private static Boolean isReconnecting = false;

    static{
        initializeHeartBeat();
    }
    public synchronized static void setIsConnected(Boolean isConnected) {
        HeartBeat.isConnected = isConnected;
    }

    public synchronized static Boolean getIsConnected() {
        return isConnected;
    }

    private synchronized static void initializeHeartBeat(){
        printStatus();
        logger.debug("Heartbeat initializing...");
        if (!getIsHeartbeatRunning()){
            Timer timer = new Timer();
            timer.schedule(new ConnectionTest(), 5000, Configuration.Setting.getHeartbeatInterval());
            setIsHeartbeatRunning(true);
            hearbeatTimer = timer;
        };

        printStatus();
    }



    public synchronized static Boolean getIsHeartbeatRunning() {
        return isHeartbeatRunning;
    }

    public synchronized static void setIsHeartbeatRunning(Boolean isHeartbeatRunning) {
        HeartBeat.isHeartbeatRunning = isHeartbeatRunning;
    }

    public synchronized static Boolean getIsReconnecting() {
        return isReconnecting;
    }

    public synchronized static void setIsReconnecting(Boolean isReconnecting) {
        HeartBeat.isReconnecting = isReconnecting;
    }

    public synchronized static Boolean getIsReconnectQueued() {
        return isReconnectQueued;
    }

    public synchronized static void setIsReconnectQueued(Boolean isReconnectQueued) {
        HeartBeat.isReconnectQueued = isReconnectQueued;
    }

    public static void printStatus(){
        logger.debug("Heartbeat is running: " + getIsHeartbeatRunning());
    }

    public static void stopHeartbeat(){
        if (getIsHeartbeatRunning()){
            hearbeatTimer.cancel();
        }
    }
}
