package com.kodz.buddy.servlets;

import com.kodz.buddy.workflows.Deploy;
import com.kodz.buddy.responseFilters.CORS;
import org.apache.maven.shared.invoker.MavenInvocationException;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import static com.kodz.buddy.workflows.Deploy.buildHistory;
import static com.kodz.buddy.workflows.Deploy.buildRepo;


@CORS
public class DeployServlet {

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("listener/{name}")
    @CORS
    public Response repoPushed(@PathParam("name") String name) throws IOException, InterruptedException, MavenInvocationException {
        Deploy repo = new Deploy();
        repo.setName(name);
        buildRepo(name);
        return Response.ok().entity(repo).build();
    }

    @GET
    @Path("/builds")
    @Produces(MediaType.APPLICATION_JSON)
    @CORS
    public Response getBuildStatus() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        String currentDateTimeStamp = dateFormat.format(cal.getTime());
        System.out.println(currentDateTimeStamp + ": getBuildStatus called");
        return Response.ok().entity(buildHistory).build();
    }

}