package com.kodz.buddy.servlets;

/**
 * Created by Kurt on 11/30/15.
 */


import com.kodz.buddy.Main;
import com.kodz.buddy.dto.StopServerResponse;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;

@Path("api")

public class StopServerProxy {

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/stop/{name}")
    public Response repoPushed(@PathParam("name") String name) throws IOException, InterruptedException {
        StopServerResponse response = new StopServerResponse();
        if (name.equals("90210")){
            try {
                  Main.stopServer();

            } catch (Exception e) {
                e.printStackTrace();
            }

            response.setMessage("Shutdown initiated, server will stop in 5 seconds.");
        }
        else {
            response.setMessage("Invalid shutdown code, server will remain running.");
        }

        return Response.ok().entity(response).build();
    }


}