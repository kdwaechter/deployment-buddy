package com.kodz.buddy.servlets;

import com.kodz.buddy.logging.DebugSocket;
import org.eclipse.jetty.websocket.servlet.WebSocketServlet;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;
import javax.servlet.annotation.WebServlet;

@SuppressWarnings("serial")
public class DebugServlet extends WebSocketServlet
{
    @Override
    public void configure(WebSocketServletFactory factory)
    {
        factory.register(DebugSocket.class);
    }
}
