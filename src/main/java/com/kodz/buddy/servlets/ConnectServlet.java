package com.kodz.buddy.servlets;

import com.kodz.buddy.config.HeartBeat;
import com.kodz.buddy.config.Helpers;
import com.kodz.buddy.dto.ConnectResponse;
import com.kodz.buddy.dto.DisconnectResponse;
import com.kodz.buddy.responseFilters.CORS;
import com.kodz.buddy.workflows.Connect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;


@CORS
@Path("api")
public class ConnectServlet {
    public static Logger logger = LoggerFactory.getLogger(ConnectServlet.class);

    @GET
    @Path("/connect")
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @CORS
    public ConnectResponse tryReconnect() throws IOException, InterruptedException {
        logger.info("Reconnect Servlet Called");
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        String currentDateTimeStamp = dateFormat.format(cal.getTime());
        HeartBeat.setIsReconnectQueued(true);
        ConnectResponse connectResponse = new ConnectResponse();
        connectResponse.setMessage("OVPN Connection Request Submitted");
        connectResponse.setTimeStamp(currentDateTimeStamp);
        Connect.reConnect(7505);
        return connectResponse;
    }

    @GET
    @Path("/disconnect")
    @Produces(MediaType.APPLICATION_JSON  + "; charset=UTF-8")
    @CORS
    public DisconnectResponse tryDisonnect() throws IOException, InterruptedException {
        logger.info("Disconnect Servlet Called");
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        String currentDateTimeStamp = dateFormat.format(cal.getTime());
        Connect.disConnect(7505);
        DisconnectResponse disconnectResponse = new DisconnectResponse();
        disconnectResponse.setTimeStamp(currentDateTimeStamp);
        disconnectResponse.setMessage("OVPN Disconnect Request Submitted");
        return disconnectResponse;

    }

}