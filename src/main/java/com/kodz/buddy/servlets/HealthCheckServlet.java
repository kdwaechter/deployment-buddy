package com.kodz.buddy.servlets;

import com.kodz.buddy.config.HeartBeat;
import com.kodz.buddy.dto.HealthCheck;
import com.kodz.buddy.responseFilters.CORS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;


@CORS
@Path("api")
public class HealthCheckServlet {
    public static Logger logger = LoggerFactory.getLogger(HealthCheckServlet.class);

    @GET
    @Path("/healthCheck")
    @Produces(MediaType.APPLICATION_JSON  + "; charset=UTF-8")
    @CORS
    public HealthCheck getHealthCheck() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        String currentDateTimeStamp = dateFormat.format(cal.getTime());

        logger.info(currentDateTimeStamp + ": healthCheck called");
        HealthCheck healthCheck = new HealthCheck();

        healthCheck.setConnected(HeartBeat.getIsConnected());
        healthCheck.setHeartbeatRunning(HeartBeat.getIsHeartbeatRunning());
        healthCheck.setReconnecting(HeartBeat.getIsReconnecting());
        healthCheck.setReconnectQueued(HeartBeat.getIsReconnectQueued());
        healthCheck.setCurrentTimeStamp(currentDateTimeStamp);

        return healthCheck;

    }

}