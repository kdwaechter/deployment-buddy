package com.kodz.buddy;


import com.kodz.buddy.config.HeartBeat;
import com.kodz.buddy.config.Helpers;
import com.kodz.buddy.config.Configuration;
import com.kodz.buddy.servlets.DebugServlet;
import com.kodz.buddy.servlets.InfoServlet;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;

/**
 * Created by Kurt on 11/16/15.
 */
public class Main {

    public static int serverPort;
    private static Server server ;

    private static void startServer(int serverPort) throws Exception {
        Main.serverPort = serverPort;
        Main.server = configureServer();
        server.setStopTimeout(3000L);
        server.start();
        server.join();
    }

    private static Server configureServer() {
        ResourceConfig resourceConfig = new ResourceConfig();
        resourceConfig.packages("com.kodz.buddy");
        resourceConfig.register(JacksonFeature.class);
        ServletContainer servletContainer = new ServletContainer(resourceConfig);
        ServletHolder restServlet = new ServletHolder(servletContainer);
        Server server = new Server();
        ServerConnector connector = new ServerConnector(server);
        connector.setPort(serverPort);
        server.addConnector(connector);
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);

        context.setContextPath("/");

        ServletHolder logInfoServlet = new ServletHolder("ws-logInfo", InfoServlet.class);
        ServletHolder logDebugServlet = new ServletHolder("ws-logDebug", DebugServlet.class);
        context.addServlet(logInfoServlet, "/log/info/*");
        context.addServlet(logDebugServlet, "/log/debug/*");
        context.addServlet(restServlet, "/*");

        server.setHandler(context);
        return server;
    }

    public static void stopServer() throws Exception {
        System.out.println("Stopping Jetty");
        try {
            // Stop the server.
            new Thread() {

                @Override
                public void run() {
                    try {
                        sleep(5000);
                        System.out.println("Shutting down Jetty...");
                        Main.server.stop();
                        System.out.println("Jetty has stopped.");
                        HeartBeat.stopHeartbeat();
                        System.out.println("Hearbeat check has stopped.");
                    } catch (Exception ex) {
                        System.out.println("Error when stopping: " + ex.getMessage());
                    }
                }
            }.start();
        } catch (Exception ex) {
            System.out.println("Unable to stop Jetty: " + ex);

        }

    }

    public static void main(String[] args) throws Exception {
        Configuration configuration = new Configuration();
        Configuration.logSettings();

        int serverPort = Configuration.Setting.getServicePort();
        Helpers.setIsWindows(Configuration.Setting.isWindows());

        HeartBeat heartBeat = new HeartBeat();

        startServer(serverPort);

    }


}