package com.kodz.buddy.dto;

/**
 * Created by Kurt on 1/22/16.
 */
public class ServerSetting {
    private int servicePort = 8888;
    private int heartbeatInterval = 10000;
    private String remoteHostDomainForTcpSocket = "kdzmkzjenkins.northcentralus.cloudapp.azure.com";
    private int remoteHostPortForTcpSocket = 8080;
    private boolean isWindows = false;

    public int getServicePort() {
        return servicePort;
    }

    public void setServicePort(int servicePort) {
        this.servicePort = servicePort;
    }

    public int getHeartbeatInterval() {
        return heartbeatInterval;
    }

    public void setHeartbeatInterval(int heartbeatInterval) {
        this.heartbeatInterval = heartbeatInterval;
    }

    public String getRemoteHostDomainForTcpSocket() {
        return remoteHostDomainForTcpSocket;
    }

    public void setRemoteHostDomainForTcpSocket(String remoteHostDomainForTcpSocket) {
        this.remoteHostDomainForTcpSocket = remoteHostDomainForTcpSocket;
    }

    public int getRemoteHostPortForTcpSocket() {
        return remoteHostPortForTcpSocket;
    }

    public void setRemoteHostPortForTcpSocket(int remoteHostPortForTcpSocket) {
        this.remoteHostPortForTcpSocket = remoteHostPortForTcpSocket;
    }

    public boolean isWindows() {
        return isWindows;
    }

    public void setIsWindows(boolean isWindows) {
        this.isWindows = isWindows;
    }

}
