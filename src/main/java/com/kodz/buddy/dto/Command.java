package com.kodz.buddy.dto;

/**
 * Created by Kurt on 1/22/16.
 */
public class Command {
    private String[] message = {"blah", "blahblah"};

    public String[] getMessage() {
        return message;
    }

    public void setMessage(String[] message) {
        this.message = message;
    }
}
